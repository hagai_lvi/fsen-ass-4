package question4.com.pais.shows;

import question4.com.pais.visitor_pattern.Visitor;

/**
 * Created by levihag on 1/5/15.
 */
public class FundedShow extends PaisShow {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
