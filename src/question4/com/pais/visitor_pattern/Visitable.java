package question4.com.pais.visitor_pattern;

/**
 * Created by levihag on 1/5/15.
 */
public interface Visitable {
    public void accept(Visitor visitor);
}
