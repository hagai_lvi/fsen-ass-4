package question4.com.pais.visitor_pattern;

import question4.com.pais.shows.ClosedShow;
import question4.com.pais.shows.FundedShow;
import question4.com.pais.shows.GeneralShow;

/**
 * There shouldn't be changes to the PaisShow classes, so this interface should remain stable
 */
public interface Visitor {

    public void visit(ClosedShow visitable);
    public void visit(FundedShow visitable);
    public void visit(GeneralShow visitable);
}
