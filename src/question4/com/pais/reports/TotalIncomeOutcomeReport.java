package question4.com.pais.reports;

import question4.com.pais.shows.ClosedShow;
import question4.com.pais.shows.FundedShow;
import question4.com.pais.shows.GeneralShow;

/**
 * Created by levihag on 1/5/15.
 */
public class TotalIncomeOutcomeReport extends PaisReport {
    @Override
    public void visit(ClosedShow visitable) {
        System.out.println("TotalIncomeOutcomeReport visiting ClosedShow");
    }

    @Override
    public void visit(FundedShow visitable) {
        System.out.println("TotalIncomeOutcomeReport visiting FundedShow");
    }

    @Override
    public void visit(GeneralShow visitable) {
        System.out.println("TotalIncomeOutcomeReport visiting GeneralShow");
    }
}
