package question4.com.pais.reports;

import question4.com.pais.shows.ClosedShow;
import question4.com.pais.shows.FundedShow;
import question4.com.pais.shows.GeneralShow;

/**
 * Adding shows Doesn't require changing other classes but only adding a class.
 */
public class NewUndefinedReport extends PaisReport {
    @Override
    public void visit(ClosedShow visitable) {
        // easy to add new classes
    }

    @Override
    public void visit(FundedShow visitable) {
        // easy to add new classes

    }

    @Override
    public void visit(GeneralShow visitable) {
        // easy to add new classes

    }
}
