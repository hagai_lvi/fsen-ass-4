package question4.com.pais.test;

import junit.framework.TestCase;
import question4.com.pais.reports.ShowsDistributionReport;
import question4.com.pais.reports.TotalFundingReport;
import question4.com.pais.reports.TotalIncomeOutcomeReport;
import question4.com.pais.shows.ClosedShow;
import question4.com.pais.shows.FundedShow;
import question4.com.pais.shows.GeneralShow;
import question4.com.pais.shows.PaisShow;
import question4.com.pais.visitor_pattern.Visitor;

/**
 * Created by levihag on 1/5/15.
 */
public class Test extends TestCase{

    Visitor showsDistributionReport = new ShowsDistributionReport();
    Visitor totalFundingReport = new TotalFundingReport();
    Visitor totalIncomeOutcomeReport = new TotalIncomeOutcomeReport();

    public void testClosedShow(){
        System.out.println("** testClosedShow **");
        PaisShow show = new ClosedShow();
        show.accept(showsDistributionReport);
        show.accept(totalFundingReport);
        show.accept(totalIncomeOutcomeReport);
        System.out.println();
    }

    public void testFundedShow(){
        System.out.println("** testFundedShow **");
        PaisShow show = new FundedShow();
        show.accept(showsDistributionReport);
        show.accept(totalFundingReport);
        show.accept(totalIncomeOutcomeReport);
        System.out.println();
    }

    public void testGeneralShow(){
        System.out.println("** testGeneralShow **");
        PaisShow show = new GeneralShow();
        show.accept(showsDistributionReport);
        show.accept(totalFundingReport);
        show.accept(totalIncomeOutcomeReport);
        System.out.println();
    }
}
