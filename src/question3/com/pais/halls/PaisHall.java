package question3.com.pais.halls;

import java.util.Iterator;

/**
 * This is our Aggregate interface
 */
public interface PaisHall {
    boolean reserveSeats(int[] seats);

    Iterator<int[]> getBestSeatsIterator(int numOfSeats);
}
