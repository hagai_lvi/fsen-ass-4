package question3.com.pais.halls;

import question3.com.pais.halls_iterators.BasicHallIterator;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class BasicHall implements PaisHall {

	public BasicHall(int numOfSeats){
		System.out.println("BasicHall constructor");
	}

	@Override
	public boolean reserveSeats(int[] seats){
		System.out.println("BasicHall.reserveSeats()");
		return false;
	}

	@Override
	public Iterator<int[]> getBestSeatsIterator(int numOfSeats) {
		return new BasicHallIterator(numOfSeats);
	}
}
