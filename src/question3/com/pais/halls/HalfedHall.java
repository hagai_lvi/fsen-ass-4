package question3.com.pais.halls;

import question3.com.pais.halls_iterators.HalfedHallIterator;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class HalfedHall implements PaisHall {

	public HalfedHall(int numOfSeats){
		System.out.println("HalfedHall constructor");
	}

	@Override
	public boolean reserveSeats(int[] seats) {
		System.out.println("HalfedHall.reserveSeats()");
		return false;
	}

	@Override
	public Iterator<int[]> getBestSeatsIterator(int numOfSeats) {
		return new HalfedHallIterator(numOfSeats);
	}
}
