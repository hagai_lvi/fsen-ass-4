package question3.com.pais.halls;

import question3.com.pais.halls_iterators.VaryingHallIterator;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class VaryingHall implements PaisHall {

	public VaryingHall(int numOfSeats){
		System.out.println("VaryingHall constructor");
	}

	@Override
	public boolean reserveSeats(int[] seats) {
		System.out.println("reserveSeats.reserveSeats()");
		return false;
	}

	@Override
	public Iterator<int[]> getBestSeatsIterator(int numOfSeats) {
		return new VaryingHallIterator(numOfSeats);
	}
}
