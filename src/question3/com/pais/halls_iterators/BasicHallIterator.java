package question3.com.pais.halls_iterators;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class BasicHallIterator implements Iterator<int[]> {

	public BasicHallIterator(int numOfSeats) {
		System.out.println("BasicHallIterator constructor");
		this.numOfSeats = numOfSeats;
	}

	int numOfSeats;


	@Override
	public boolean hasNext() {
		System.out.println("BasicHallIterator.hasNext()");
		return false;
	}

	@Override
	public int[] next() {
		System.out.println("BasicHallIterator.next()");
		return new int[0];
	}

	@Override
	public void remove() {
		System.out.println("BasicHallIterator.remove()");
	}
}
