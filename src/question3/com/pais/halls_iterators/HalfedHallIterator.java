package question3.com.pais.halls_iterators;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class HalfedHallIterator implements Iterator<int[]> {

	public HalfedHallIterator(int numOfSeats) {
		System.out.println("HalfedHallIterator constructor");
		this.numOfSeats = numOfSeats;
	}

	int numOfSeats;

	@Override
	public boolean hasNext() {
		System.out.println("HalfedHallIterator.hasNext()");
		return false;
	}

	@Override
	public int[] next() {
		System.out.println("HalfedHallIterator.next()");
		return new int[0];
	}

	@Override
	public void remove() {
		System.out.println("HalfedHallIterator.remove()");
	}
}
