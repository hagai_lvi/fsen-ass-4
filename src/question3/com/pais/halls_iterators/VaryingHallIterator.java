package question3.com.pais.halls_iterators;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/6/15.
 */
public class VaryingHallIterator implements Iterator<int[]> {

	int numOfSeats;

	public VaryingHallIterator(int numOfSeats){
		System.out.println("VaryingHallIterator constructor");
		this.numOfSeats = numOfSeats;
	}

	@Override
	public boolean hasNext() {
		System.out.println("VaryingHallIterator.hasNext()");
		return false;
	}

	@Override
	public int[] next() {
		System.out.println("VaryingHallIterator.next()");
		return new int[0];
	}

	@Override
	public void remove() {
		System.out.println("VaryingHallIterator.remove()");
	}
}
