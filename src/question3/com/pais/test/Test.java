package question3.com.pais.test;

import question3.com.pais.halls.BasicHall;
import question3.com.pais.halls.HalfedHall;
import question3.com.pais.halls.PaisHall;
import question3.com.pais.halls.VaryingHall;

import java.util.Iterator;

/**
 * Created by hagai_lvi on 1/10/15.
 */
public class Test  {

	public static final int NUM_OF_SEATS = 100;

	public static void main(String[] args) {
		testBasicHall();
		testHalfedHall();
		testVaryingHall();
	}

	private static void testBasicHall() {
		System.out.println("** testBasicHall **");
		genericTest(new BasicHall(NUM_OF_SEATS), 5);
	}

	private static void testHalfedHall() {
		System.out.println("** testHalfedHall **");
		genericTest(new HalfedHall(NUM_OF_SEATS), 5);
	}

	private static void testVaryingHall() {
		System.out.println("** testVaryingHall **");
		genericTest(new VaryingHall(NUM_OF_SEATS), 5);
	}

	public static void genericTest(PaisHall hall, int numOfSeats) {
		boolean keepGoing = true;
		Iterator it = hall.getBestSeatsIterator(numOfSeats);
		for (int i = 0 ; i < 5 && keepGoing ; i++){
			if (keepGoing = it.hasNext()){
				it.next();
			}
		}
	}
}
